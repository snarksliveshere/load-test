package main

import (
	"fmt"
	"sync"
	"time"

	"gitlab.com/snarksliveshere/load-test/internal/config"
	load2 "gitlab.com/snarksliveshere/load-test/internal/load"
)

func main() {
	cfg := config.Init()

	load := load2.NewLoad(cfg)
	now := time.Now()
	var wg sync.WaitGroup
	for i := 0; i < cfg.RPS; i++ {
		wg.Add(1)
		go func(num int) {
			load.LoadWorker(&wg, num)
		}(i)
	}
	wg.Wait()
	fmt.Println(time.Since(now).Seconds(), "seconds")
}
