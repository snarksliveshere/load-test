package config

import (
	"fmt"
	"time"

	"github.com/kelseyhightower/envconfig"
)

type Config struct {
	TestHostHTTP    string        `envconfig:"TEST_HOST_HTTP" default:"localhost:8088"`
	TestHostTimeout time.Duration `envconfig:"TEST_HOST_TIMEOUT" default:"10s"`

	DefaultSchema string `envconfig:"DEFAULT_SCHEMA" default:"http"`

	RPS     int `envconfig:"TEST_RPS"`
	RPSTime int `envconfig:"TEST_RPS_TIME"`

	ServerEnv string `envconfig:"TEST_SERVER_ENV" default:"dev"`
	NumState  int    `envconfig:"TEST_STATE_NUM" default:"3"`
	NumPut    int    `envconfig:"TEST_PUT_NUM" default:"3"`
	NumGet    int    `envconfig:"TEST_GET_NUM" default:"3"`

	RandLength int `envconfig:"TEST_RAND_LENGTH" default:"10000"`
}

func Init() *Config {
	var cfg Config
	err := envconfig.Process("test", &cfg)
	if err != nil {
		panic(fmt.Errorf("error on loading config: %s", err))
	}
	return &cfg
}
