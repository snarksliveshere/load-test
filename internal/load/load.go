package load

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"math/rand"
	"net/http"
	"sync"
	"time"

	"gitlab.com/snarksliveshere/load-test/internal/config"
	"gitlab.com/snarksliveshere/load-test/internal/dto"
)

type Loadtest struct {
	env *config.Config
}

func NewLoad(cfg *config.Config) *Loadtest {
	return &Loadtest{
		env: cfg,
	}
}

func (test *Loadtest) LoadWorker(wg *sync.WaitGroup, num int) {
	defer wg.Done()
	for i := 0; i < test.env.RPSTime; i++ {
		now := time.Now().UTC()
		err := test.loadJob()
		if err != nil {
			fmt.Println("error worker", err.Error())
		}
		fmt.Println(time.Since(now).Milliseconds(), "ms")
		time.Sleep(300 * time.Millisecond)
	}
}

var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890")

func getRandomString() string {
	b := make([]rune, rand.Intn(len(letters)))
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

func (test *Loadtest) loadJob() error {

	return nil
}

func (test *Loadtest) Call(ctx context.Context, client http.Client, req dto.Request, cookies []*http.Cookie) (*http.Response, []byte, error) {
	var request *http.Request
	request, err := http.NewRequest(req.Method, req.URL, bytes.NewReader(req.Data))
	if err != nil {
		return nil, nil, err
	}
	request = request.WithContext(ctx)
	if req.Method != http.MethodGet {
		request.Header.Set("Content-Type", "application/json")
	}
	if len(req.Headers) > 0 {
		for k, v := range req.Headers {
			request.Header.Set(k, v)
		}
	}
	if len(cookies) > 0 {
		for _, v := range cookies {
			request.AddCookie(v)
		}
	}
	var resp *http.Response
	resp, err = client.Do(request)
	if resp != nil {
		defer func() { _ = resp.Body.Close() }()
	}
	if err != nil {
		return nil, nil, err
	}
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, nil, fmt.Errorf(err.Error())
	}
	return resp, body, nil
}
