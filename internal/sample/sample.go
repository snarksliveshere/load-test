package sample

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"time"

	"gitlab.com/snarksliveshere/load-test/internal/client"
	"gitlab.com/snarksliveshere/load-test/internal/dto"
)

const (
	defaultTimeout = 5 * time.Second
	host           = "localhost:8087"
)

var (
	jd = `{
              "type": "request",
              "user_id": "wednesday_my_dudes",
              "value": [{"olala": "this_name", "vals": 21, "password": "firstpass"}, {"sec": true}],
              "event_name": "olala_dudes"
      }`

	simpleHeaders = `{
		"Content-Type": "application/json"
	}`
)

func Sample(pathStr string) {
	hc := http.Client{
		Timeout: defaultTimeout,
	}

	b, err := getRequest()
	if err != nil {
		log.Fatal(err)
	}

	req := dto.Request{}
	req.Data = b
	req.Method = http.MethodPost
	path := fmt.Sprintf(pathStr)
	req.URL = FormURL("http", host, path)
	err = json.Unmarshal([]byte(simpleHeaders), &req.Headers)
	if err != nil {
		log.Fatal(err)
	}

	resp, body, err := client.Call(context.Background(), hc, req)
	if err != nil {
		log.Fatal("cannot set request", err)
	}
	fmt.Println(resp)
	fmt.Println(string(body))
}

func getRequest() ([]byte, error) {
	m := make(map[string]interface{})

	err := json.Unmarshal([]byte(jd), &m)
	if err != nil {
		log.Fatal(err)
	}
	res := make(map[string][]map[string]interface{})
	t := time.Now()
	m["update_time"] = t.Unix()
	m["event_time"] = t.Unix() + 10
	res["data"] = append([]map[string]interface{}(nil), m)
	return json.Marshal(res)
}

func FormURL(scheme, host, path string) string {
	u := url.URL{
		Scheme: scheme,
		Host:   host,
		Path:   path,
	}
	return u.String()
}
