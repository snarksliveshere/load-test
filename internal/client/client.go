package client

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"net/http"

	"gitlab.com/snarksliveshere/load-test/internal/dto"
)

func Call(ctx context.Context, client http.Client, req dto.Request) (*http.Response, []byte, error) {
	var request *http.Request
	request, err := http.NewRequest(req.Method, req.URL, bytes.NewReader(req.Data))
	if err != nil {
		return nil, nil, err
	}
	request = request.WithContext(ctx)
	if req.Method != http.MethodGet {
		request.Header.Set("Content-Type", "application/json")
	}
	if len(req.Headers) > 0 {
		for k, v := range req.Headers {
			request.Header.Set(k, v)
		}
	}
	var resp *http.Response
	resp, err = client.Do(request)
	if resp != nil {
		defer func() { _ = resp.Body.Close() }()
	}
	if err != nil {
		return nil, nil, err
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, nil, fmt.Errorf(err.Error())
	}
	return resp, body, nil
}
