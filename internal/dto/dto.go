package dto

import (
	"context"
	"fmt"
	"log"
	"os"
)

type Request struct {
	Data    []byte
	Method  string
	URL     string
	Ctx     context.Context
	Headers map[string]string
}

func Write2File(s string, filename string) {
	f, err := os.OpenFile(filename,
		os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatalln(err)
	}
	defer f.Close()
	if _, err := f.WriteString(fmt.Sprintf("%s\n", s)); err != nil {
		log.Fatalln(err)
	}
}
